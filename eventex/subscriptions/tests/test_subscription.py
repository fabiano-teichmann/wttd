from django.core import mail
from django.test import TestCase
from eventex.subscriptions.forms import SubscriptionForm

class SubriscibeTest(TestCase):
    def setUp(self):
        self.resp = self.client.get('/inscricao/')
    def test_get(self):
        """Get /inscricao/ must return status code 200"""
        self.assertEquals(200, self.resp.status_code)

    def test_template(self):
        """Must use subscriptions/subscription_form.html"""
        self.assertTemplateUsed(self.resp, 'subscriptions/subscription_form.html')

    def test_html(self):
        """Html must contain input tags"""
        tags = (
                ('<form>', 0),
                ('input', 6),
                ('type="text"', 3),
                ('type="email"', 1),
                ('type="submit"', 1)
                )
        for text, count in tags:
            with self.subTest():
                self.assertContains(self.resp, text, count)


    def test_csrf(self):
        """HTML must contain  csrf"""
        self.assertContains(self.resp, 'csrfmiddlewaretoken')

    def test_has_form(self):
        """Context  must have subscription"""
        form = self.resp.context['form']
        self.assertIsInstance(form, SubscriptionForm)

    def test_has_form_fields(self):
        """Form must have 4 fields"""
        form = self.resp.context['form']
        self.assertSequenceEqual(['name', 'cpf', 'email', 'phone'], list(form.fields))


class SubscribePostTest(TestCase):
    def setUp(self):
        data = dict(name='Fabiano Teichmann', cpf='00487917090',
                    email='fabiano.geek@gmail.com', phone='47-99443996')
        response = self.client.post('/inscricao/', data)
        self.resp = response


    def test_post(self):
        """Valid post send subribe"""
        self.assertEqual(302, self.resp.status_code)

    def test_send_subscribe_email(self):
        """Test send email subscribe"""
        self.assertEqual(1, len(mail.outbox))

    def test_subricription_email_subject(self):
        email = mail.outbox[0]
        expect = 'Confirmação inscrição'
        self.assertEqual(expect, email.subject)

    def test_subcription_email_from(self):
        email = mail.outbox[0]
        expect = 'contato@eventex.com'
        self.assertEqual(expect, email.from_email)

    def test_subcription_email_to(self):
        email = mail.outbox[0]
        expect = ['contato@eventex.com', 'fabiano.geek@gmail.com']
        self.assertEqual(expect, email.to)

    def test_subscription_email_body(self):
        email = mail.outbox[0]
        self.assertIn('Fabiano Teichmann', email.body)
        self.assertIn('00487917090', email.body)
        self.assertIn('fabiano.geek@gmail.com', email.body)
        self.assertIn('47-99443996', email.body)


class SubscribeInvalidPost(TestCase):
    def setUp(self):
        self.resp = self.client.post('/inscricao/', {})

    def test_post(self):
        """Invalid  POST should  not redirect"""
        self.assertEqual(200,  self.resp.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.resp, 'subscriptions/subscription_form.html')

    def test_has_subscription(self):
        form = self.resp.context['form']
        self.assertIsInstance(form, SubscriptionForm)

    def test_has_error(self):
        form = self.resp.context['form']
        self.assertTrue(form.errors)